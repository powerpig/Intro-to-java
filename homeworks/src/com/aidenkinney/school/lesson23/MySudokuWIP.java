package com.aidenkinney.school.lesson23;

import java.util.*;

/**
 * @author Aiden
 */
public class MySudokuWIP {

    public static void main(String[] args)
    {
    	double Grid11 = 0;
    	double Grid12 = 0;
    	double Grid13 = 0;
    	double Grid14 = 0;
    	double Grid15 = 0;
    	double Grid16 = 0;
    	double Grid17 = 0;
    	double Grid18 = 0;
    	double Grid19 = 0;
    	
    	double Grid31 = 0;
    	double Grid32 = 0;
    	double Grid33 = 0;
    	double Grid34 = 0;
    	double Grid35 = 0;
    	double Grid36 = 0;
    	double Grid37 = 0;
    	double Grid38 = 0;
    	double Grid39 = 0;
    	
    	double Grid51 = 0;
    	double Grid52 = 0;
    	double Grid53 = 0;
    	double Grid54 = 0;
    	double Grid55 = 0;
    	double Grid56 = 0;
    	double Grid57 = 0;
    	double Grid58 = 0;
    	double Grid59 = 0;
    	
    	double Grid61 = 0;
    	double Grid62 = 0;
    	double Grid63 = 0;
    	double Grid64 = 0;
    	double Grid65 = 0;
    	double Grid66 = 0;
    	double Grid67 = 0;
    	double Grid68 = 0;
    	double Grid69 = 0;
    	
    	double Grid71 = 0;
    	double Grid72 = 0;
    	double Grid73 = 0;
    	double Grid74 = 0;
    	double Grid75 = 0;
    	double Grid76 = 0;
    	double Grid77 = 0;
    	double Grid78 = 0;
    	double Grid79 = 0;
    	//answers
    	//TopGrids
    	/*
    	System.out.println(" 9 8 1 : 2 3 7 : 5 4 6"
    						, Grid11, Grid12, Grid13
    						, Grid31, Grid32, Grid33);//Row1
    	System.out.println(" 6 2 4 : 8 5 1 : 3 9 7"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.println(" 5 7 3 : 6 4 9 : 1 2 8"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	
    	System.out.println(" X X X X X X X X X X X")//barrier
    	//MidGrids
    	System.out.println(" 7 4 2 : 5 9 6 : 8 3 1"
							, Grid11, Grid12, Grid13
							, Grid31, Grid32, Grid33);//Row1
    	System.out.println(" 8 3 5 : 7 1 2 : 9 6 4"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.println(" 1 9 6 : 3 8 4 : 7 5 2"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2

    	System.out.println(" X X X X X X X X X X X")//barrier
    	//BotGrids
    	System.out.println(" 3 6 9 : 1 2 8 : 4 7 5"
							, Grid11, Grid12, Grid13
							, Grid31, Grid32, Grid33);//Row1
    	System.out.println(" 2 5 8 : 4 7 3 : 6 1 9"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.println(" 4 1 7 : 9 6 5 : 2 8 3"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2

    	System.out.println(" X X X X X X X X X X X")//barrier
		*/	
    	System.out.printf(" _ _ _ : 2 3 7 : _ _ _\n"
							, Grid11, Grid12, Grid13
							, Grid31, Grid32, Grid33);//Row1
    	System.out.printf(" _ _ _ : 8 5 1 : _ _ _\n"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.printf(" _ _ _ : 6 4 9 : _ _ _\n"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2

    	System.out.printf(" X X X X X X X X X X X\n");//barrier
    			//MidGrids
    	System.out.printf(" 7 4 2 : _ _ _ : _ _ _\n"
							, Grid51, Grid52, Grid53
							, Grid61, Grid62, Grid63);//Row1
    	System.out.printf(" 8 3 5 : _ _ _ : _ _ _\n"
							, Grid54, Grid55, Grid56
							, Grid64, Grid65, Grid66);//Row2
    	System.out.printf(" 1 9 6 : _ _ _ : _ _ _\n"
    						, Grid54, Grid55, Grid56
    						, Grid64, Grid65, Grid66);//Row2

    	System.out.printf(" X X X X X X X X X X X\n");//barrier
    			//BotGrids
    	System.out.printf(" _ _ _ : 1 2 8 : 4 7 5\n"
							, Grid11, Grid12, Grid13
							, Grid31, Grid32, Grid33);//Row1
    	System.out.printf(" _ _ _ : 4 7 3 : 6 1 9\n"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.printf(" _ _ _ : 9 6 5 : 2 8 3\n"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2

    	System.out.printf(" X X X X X X X X X X X\n");//barrier
    	
    	Scanner input = new Scanner(System.in);
    	//Grid1 input
    	System.out.println("Grid1 Slot1");
    	Grid11 = input.nextDouble();
    	System.out.println("Grid1 Slot2");
    	Grid12 = input.nextDouble();
    	System.out.println("Grid1 Slot3");
    	Grid13 = input.nextDouble();
    	System.out.println("Grid1 Slot4");
    	Grid14 = input.nextDouble();
    	System.out.println("Grid1 Slot5");
    	Grid15 = input.nextDouble();
    	System.out.println("Grid1 Slot6");
    	Grid16 = input.nextDouble();
    	System.out.println("Grid1 Slot7");
    	Grid17 = input.nextDouble();
    	System.out.println("Grid1 Slot8");
    	Grid18 = input.nextDouble();
    	System.out.println("Grid1 Slot9");
    	Grid19 = input.nextDouble();
    	
    	System.out.printf(" %d %d %d : 2 3 7 : _ _ _\n"
							, Grid11, Grid12, Grid13
							, Grid31, Grid32, Grid33);//Row1
    	System.out.printf(" %d %d %d : 8 5 1 : _ _ _\n"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.printf(" %d %d %d : 6 4 9 : _ _ _\n"
							, Grid17, Grid18, Grid19
							, Grid37, Grid38, Grid39);//Row2
    	
    	//Grid2 input
    	System.out.println("Grid3 Slot1");
    	Grid31 = input.nextDouble();
    	System.out.println("Grid3 Slot2");
    	Grid32 = input.nextDouble();
    	System.out.println("Grid3 Slot3");
    	Grid33 = input.nextDouble();
    	System.out.println("Grid3 Slot4");
    	Grid34 = input.nextDouble();
    	System.out.println("Grid3 Slot5");
    	Grid35 = input.nextDouble();
    	System.out.println("Grid3 Slot6");
    	Grid36 = input.nextDouble();
    	System.out.println("Grid3 Slot7");
    	Grid37 = input.nextDouble();
    	System.out.println("Grid3 Slot8");
    	Grid38 = input.nextDouble();
    	System.out.println("\nGrid3 Slot9");
    	Grid39 = input.nextDouble();
    	
    	System.out.printf(" %d %d %d : 2 3 7 : %d %d %d\n"
							, Grid11, Grid12, Grid13
							, Grid31, Grid32, Grid33);//Row1
    	System.out.printf(" %d %d %d : 8 5 1 : %d %d %d\n"
							, Grid14, Grid15, Grid16
							, Grid34, Grid35, Grid36);//Row2
    	System.out.printf(" %d %d %d : 6 4 9 : %d %d %d\n"
							, Grid17, Grid18, Grid19
							, Grid37, Grid38, Grid39);//Row2
    	
    }
}